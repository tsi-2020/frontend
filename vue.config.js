// const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;

module.exports = {
  publicPath: process.env.BASE_URL ? process.env.BASE_URL : "/",
  transpileDependencies: ["vuetify"]
  // configureWebpack: {
  //   plugins: [new BundleAnalyzerPlugin()]
  // }
};
