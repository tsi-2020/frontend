import { Vue, Component } from "vue-property-decorator";

@Component({})
export class RulesMixin extends Vue {
  public rules = {
    required: (value: string) => !!value || "Este campo es obligatorio",
    minPassword: (v: string) => v.length >= 8 || "Minimo 8 caracteres",
    mail: (value: string) => {
      const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return pattern.test(value) || "Ingrese un email valido";
    },
    fileSize: (value: File) => !value || value.size < 1000000 || "El tamaño de la imagen debe ser menor a 1 MB"
  };

  public onlyNumber(event: KeyboardEvent) {
    const target: HTMLInputElement = event.target as HTMLInputElement;
    const pattern = /[0-9]/;
    if (target.value.length > 8 || !pattern.test(event.key)) {
      event.preventDefault();
    }
  }
}
