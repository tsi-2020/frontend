import { Vue, Component } from "vue-property-decorator";
import { AxiosError } from "axios";

@Component({})
export class NotificationsMixin extends Vue {
  public success(title: string, text: string) {
    this.$notify({
      group: "feedback",
      duration: 5000,
      title: title,
      text: text,
      type: "success"
    });
  }

  public error(title: string, text: string) {
    this.$notify({
      group: "feedback",
      duration: 5000,
      title: title,
      text: text,
      type: "error"
    });
  }

  public errorHandling(error: AxiosError) {
    if (error.isAxiosError && error.response) {
      if (error.response.status === 500) {
        this.$router.push({ name: "Server Error" });
        this.error("Error", "Oops algo salio mal :(");
      } else if (error.response.status === 404) {
        this.$router.push({ name: "404", params: { "0": "" } });
        this.error("Error", error.response.data.message);
      } else {
        this.error("Error", error.response.data.message);
      }
    } else {
      this.$router.push({ name: "404", params: { "0": "" } });
      this.error("Error", "Oops algo salio mal :(");
    }
  }
}
