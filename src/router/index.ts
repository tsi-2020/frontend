import Vue from "vue";
import VueRouter, { RouteConfig, Route } from "vue-router";
import store from "@/store/index";
import { getUniversityByPath } from "@/services/universities";
import Vuetify from "@/plugins/vuetify";
import { updateHeaders } from "@/plugins/axios";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "Landing",
    component: () => import("@/views/Landing.vue"),
    meta: {
      requireAuth: false
    }
  },
  {
    path: "/resetPassword/:token",
    name: "Reset Password",
    component: () => import("@/views/ResetPassword.vue"),
    meta: {
      requireAuth: false
    }
  },
  {
    path: "/invalidToken",
    name: "Invalid Token",
    component: () => import("@/views/InvalidToken.vue"),
    meta: {
      requireAuth: false
    }
  },
  {
    path: "/:university/",
    name: "Home",
    component: () => import("@/views/Home.vue"),
    meta: {
      layout: "HeaderLayout",
      requireAuth: false
    }
  },
  {
    path: "/:university/login",
    name: "Login",
    component: () => import("@/views/Login.vue"),
    meta: {
      requireAuth: false
    }
  },
  {
    path: "/:university/courses",
    name: "Courses",
    component: () => import("@/views/CoursesList.vue"),
    meta: {
      layout: "HeaderLayout",
      requireAuth: false
    }
  },
  {
    path: "/:university/profile",
    name: "Profile",
    component: () => import("@/views/Profile.vue"),
    beforeEnter: (to, _from, next) => {
      if (!Vuetify.framework.breakpoint.xsOnly) {
        next({ name: "404", params: { "0": to.path } });
      }
      next();
    },
    meta: {
      layout: "HeaderLayout",
      requireAuth: true
    }
  },
  {
    path: "/:university/careers",
    name: "CareersList",
    component: () => import("@/views/CareersList.vue"),
    meta: {
      layout: "HeaderLayout",
      requireAuth: false
    }
  },
  {
    path: "/:university/chats",
    name: "Chats",
    component: () => import("@/views/Chats.vue"),
    meta: {
      layout: "HeaderLayout",
      requireAuth: true
    }
  },
  {
    path: "/:university/polls",
    name: "UniversityPolls",
    component: () => import("@/views/Polls.vue"),
    meta: {
      layout: "HeaderLayout",
      requireAuth: true
    }
  },
  {
    path: "/:university/mycourses",
    name: "MyCourses",
    component: () => import("@/views/MyCourses.vue"),
    meta: {
      layout: "HeaderLayout",
      requireAuth: true
    }
  },
  {
    path: "/:university/polls/:poll",
    name: "PollUniversity",
    component: () => import("@/views/Course/Poll.vue"),
    meta: {
      layout: "HeaderLayout",
      requireAuth: true
    }
  },
  {
    path: "/:university/notices/:notice",
    name: "Notice",
    component: () => import("@/views/Notice.vue"),
    meta: {
      layout: "HeaderLayout",
      requireAuth: false
    }
  },
  {
    path: "/:university/chats/:chat",
    name: "Chat",
    component: () => import("@/views/Chat.vue"),
    meta: {
      layout: "HeaderLayout",
      requireAuth: true
    }
  },
  {
    path: "/:university/course/:course",
    name: "Course",
    component: () => import("@/views/Course.vue"),
    meta: {
      layout: "HeaderLayout",
      requireAuth: true
    }
  },
  {
    path: "/:university/careers/:career/courses",
    name: "CareerCoursesList",
    component: () => import("@/views/CareerCoursesList.vue"),
    meta: {
      layout: "HeaderLayout",
      requireAuth: false
    }
  },
  {
    path: "/:university/course/:course/polls",
    name: "Polls",
    component: () => import("@/views/Course/Polls.vue"),
    meta: {
      layout: "CourseLayout",
      requireAuth: true
    }
  },
  {
    path: "/:university/course/:course/polls/:poll",
    name: "Poll",
    component: () => import("@/views/Course/Poll.vue"),
    meta: {
      layout: "CourseLayout",
      requireAuth: true
    }
  },
  {
    path: "/:university/course/:course/assignments/:assignment",
    name: "Assignment",
    component: () => import("@/views/Course/Assignment.vue"),
    meta: {
      layout: "CourseLayout",
      requireAuth: true
    }
  },
  {
    path: "/:university/course/:course/polls/:poll/answers",
    name: "PollAnswers",
    component: () => import("@/views/Course/PollAnswers.vue"),
    meta: {
      layout: "CourseLayout",
      requireAuth: true
    }
  },
  {
    path: "/:university/course/:course/addPoll",
    name: "AddPoll",
    component: () => import("@/views/Course/AddPoll.vue"),
    meta: {
      layout: "CourseLayout",
      requireAuth: true
    }
  },
  {
    path: "/:university/course/:course/students",
    name: "StudentsList",
    component: () => import("@/views/StudentsList.vue"),
    meta: {
      layout: "CourseLayout",
      requireAuth: true
    }
  },
  {
    path: "/:university/course/:course/professors",
    name: "ProfessorsList",
    component: () => import("@/views/ProfessorsList.vue"),
    meta: {
      layout: "CourseLayout",
      requireAuth: true
    }
  },
  {
    path: "/:university/course/:course/forums/:forum",
    name: "Forum",
    component: () => import("@/views/Course/Forum.vue"),
    meta: {
      layout: "CourseLayout",
      requireAuth: true
    }
  },
  {
    path: "/:university/course/:course/threads/:thread",
    name: "Thread",
    component: () => import("@/views/Course/Thread.vue"),
    meta: {
      layout: "CourseLayout",
      requireAuth: true
    }
  },
  {
    path: "/:university/course/:course/calendar",
    name: "Calendar",
    component: () => import("@/views/Course/Calendar.vue"),
    meta: {
      layout: "HeaderLayout",
      requireAuth: true
    }
  },
  {
    path: "*",
    name: "404",
    component: () => import("@/views/NotFound.vue"),
    meta: {
      requireAuth: false
    }
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

async function verifyUniversity(to: Route): Promise<string> {
  const university = await getUniversityByPath(`/${to.params.university}`);
  Vuetify.framework.theme.themes.light.primary = university.primaryColor;
  Vuetify.framework.theme.themes.light.secondary = university.secondaryColor;
  const universityIsNotSet = !store.state.university?.id ?? true;
  const universityHasChanged = store.state.university?.id != university.id;

  if (universityIsNotSet || universityHasChanged) {
    store.commit("SET_UNIVERSITY", university);
    window.document.title = university.name;
    if (store.state.userModule?.sessions[university.id]) {
      updateHeaders(store.state.userModule.sessions[university.id].tokens.accessToken);
      store.commit("userModule/SET_SESSION", store.state.userModule?.sessions[university.id]);
    } else {
      store.commit("userModule/CLEAR_ACTIVE_SESSION");
    }
  }
  return university.accessPath;
}

router.beforeEach((to, _from, next) => {
  store.commit("userModule/SET_IN_LOGIN", to.name === "Login");
  if (to.params.university) {
    verifyUniversity(to)
      .then((path: string) => {
        const loggedIn = store.state.userModule?.session?.user;
        if (to.matched.some(record => record.meta.requireAuth) && !loggedIn) {
          next(`${path}/login`);
        } else if (loggedIn && to.name == "Login") {
          next(path);
        }
      })
      .catch(() => {
        next({ name: "404", params: { "0": to.path } });
      });
  }
  next();
});

export default router;
