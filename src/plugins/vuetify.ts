import Vue from "vue";
import Vuetify, { VRow, VCol, VImg, VCard, VTextField, VTextarea, VFileInput } from "vuetify/lib";

Vue.use(Vuetify, {
  components: {
    VRow,
    VCol,
    VImg,
    VCard,
    VTextField,
    VTextarea,
    VFileInput
  }
});

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: "#2196F3",
        secondary: "#00bcd4",
        accent: "#82B1FF",
        error: "#F76358",
        info: "#2196F3",
        success: "#619d4b",
        warning: "#f9a825"
      }
    },
    options: {
      customProperties: true
    }
  }
});
