import firebase from "firebase/app";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: process.env.VUE_APP_APIKEY,
  authDomain: process.env.VUE_APP_AUTHDOMAIN,
  databaseURL: process.env.VUE_APP_DB,
  projectId: process.env.VUE_APP_PID,
  storageBucket: process.env.VUE_APP_SB,
  messagingSenderId: process.env.VUE_APP_SID,
  appId: process.env.VUE_APP_APPID,
  measurementId: process.env.VUE_APP_MID
};

firebase.initializeApp(firebaseConfig);

export default firebase.firestore.Timestamp;

export const getServerDate = () => {
  return firebase.firestore.Timestamp.now();
};

export const toDate = (date: firebase.firestore.Timestamp) => {
  return date.toDate();
};

export const firestore = firebase.firestore();
