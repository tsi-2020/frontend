import { Course } from "@/models/course";

export type Career = {
  id: string;
  name: string;
  subjects: Course[];
};
