import Timestamp from "@/plugins/firebase";

export type Chat = {
  userA: string;
  userB: string;
  userAId: string;
  userBId: string;
  displayName: string;
  id: string;
  lastMessage: string;
  updatedAt: Timestamp;
};
