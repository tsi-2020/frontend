import { EducationalUnit } from "@/models/educationalUnit";
export type Course = {
  id: string;
  name: string;
  validateWithBedelia: boolean;
  isRemote: boolean;
  components?: EducationalUnit[];
};
