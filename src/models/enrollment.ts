export type ProfessorsEnrollments = {
  id: string;
  user: {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    ci: 0;
    roles: string[];
  };
  isResponsible: true;
};

export type StudentsEnrollments = {
  id: string;
  user: {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    ci: 0;
    roles: string[];
  };
};

export type Enrollment = {
  id: string;
  isProfessorEnrollment: boolean;
  name: string;
  score?: number;
};
