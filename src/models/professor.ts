export type GetProfessor = {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  password?: string;
  ci: string;
  roles?: string[];
};

export type UpdateProfessor = {
  firstName: string;
  lastName: string;
  email: string;
};
