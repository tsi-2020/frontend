export type Forum = {
  id: string;
  name: string;
  threads?: [
    {
      id: string;
      title: string;
    }
  ];
};
