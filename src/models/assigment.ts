export type Assigment = {
  id: string;
  availableFrom: string;
  availableUntil: string;
  maxScore: number;
  text: string;
  title: string;
  deliveredAssigment: {
    id: string;
    score: number;
    fileUrl: string;
    fileName: string;
    professorComments: string;
  };
};
