export type Thread = {
  id: string;
  title: string;
};

export type GetThreads = {
  count: number;
  threadList: {
    id: string;
    title: string;
  }[];
};

export type GetMessages = {
  count: number;
  messageList: [
    {
      userId: string;
      text: string;
      fullName: string;
      createdOn: string;
    }
  ];
};

export type Message = {
  id?: string;
  userId: string;
  text: string;
  fullName: string;
  createdOn: string;
};
