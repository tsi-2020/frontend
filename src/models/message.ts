export type Message = {
  id: string;
  chatId: string;
  createdAt: Date;
  from: string;
  text: string;
};
