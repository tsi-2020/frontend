export type User = {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  ci: number;
  roles: string[];
};
