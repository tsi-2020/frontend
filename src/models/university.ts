export type University = {
  id: string;
  name: string;
  accessPath: string;
  administrators?: [];
  logo?: string;
  primaryColor?: string;
  secondaryColor?: string;
};
