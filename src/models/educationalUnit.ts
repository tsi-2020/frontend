export type EducationalUnit = {
  id?: string;
  position: number;
  title: string;
  text: string;
  assigmentComponents:
    | [
        {
          position: number;
          title: string;
          text: string;
          assignmentId: string;
          availableUntil: string;
        }
      ]
    | [];
  documentComponents?:
    | [
        {
          position: number;
          title: string;
          url: string;
        }
      ]
    | [];
  forumComponents?:
    | [
        {
          position: number;
          title: string;
          forumId: string;
        }
      ]
    | [];
  linkComponents?:
    | [
        {
          position: number;
          title: string;
          url: string;
        }
      ]
    | [];
  pollComponents?:
    | [
        {
          position: number;
          title: string;
          text: string;
          pollId: string;
        }
      ]
    | [];
  quizComponents?:
    | [
        {
          position: number;
          title: string;
          text: string;
          quizId: string;
        }
      ]
    | [];
  textComponents?:
    | [
        {
          position: number;
          text: string;
        }
      ]
    | [];
  videoComponents?:
    | [
        {
          position: number;
          title: string;
          url: string;
        }
      ]
    | [];
  virtualMeetingComponents?:
    | [
        {
          position: number;
          title: string;
          url: string;
        }
      ]
    | [];
};
