export type Notice = {
  id: string;
  message: string;
  createdOn: string;
  title: string;
};
