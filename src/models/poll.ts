export type Poll = {
  id: string;
  title: string;
  createdOn: string;
  position: number;
  isAnswered?: boolean;
};

export type NewPollType = {
  id?: string;
  title: string;
  openQuestions: Array<{ id?: string; text: string; position: number }>;
  multipleChoiceQuestions: Array<{
    id?: string;
    text: string;
    position: number;
    isMultipleSelect: boolean;
    choices: Array<{ id?: string; text: string }>;
  }>;
};

export type PollAnswer = {
  title: string;
  createdOn: string;
  openAnswerDtos: Array<{ question: string; position: number; response: string }>;
  multipleChoiceAnswerDtos: Array<{
    question: string;
    position: number;
    choices: Array<string>;
  }>;
};

export type ReplyPoll = {
  openQuestions: { openQuestionid: string; response: string }[];
  multipleChoiceQuestions: { multipleChoiceQuestionId: string; choices: string[] }[];
};
