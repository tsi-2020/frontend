export type CreateEvent = {
  startDate: Date;
  endDate: Date;
  event: string;
};

export type Event = {
  id: string;
  subjectId: Date;
  startDate: string;
  endDate: string;
  event: string;
  deletedOn: Date;
  createdOn: Date;
  modifiedOn: Date;
};
