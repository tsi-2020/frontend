import { Module, VuexModule, Mutation } from "vuex-module-decorators";
import { Enrollment } from "@/models/enrollment";

export type Session = {
  tokens: { accessToken: string; refreshToken: string };
  user: {
    Tenant: string;
    email: string;
    exp: number;
    given_name: string;
    iat: number;
    nameid: string;
    nbf: number;
    role: string;
  };
  enrollments: Enrollment[];
};

type Sessions = {
  [key: string]: Session;
};

@Module({ name: "userModule", namespaced: true })
export default class User extends VuexModule {
  inLogin = true;
  session: null | Session = null;
  sessions: Sessions = {};

  @Mutation
  SET_SESSION(value: Session) {
    this.sessions[value.user.Tenant] = value;
    this.session = value;
  }

  @Mutation
  CLEAR_SESSION(value: string) {
    if (this.sessions) delete this.sessions[value];
    this.session = null;
  }

  @Mutation
  CLEAR_ACTIVE_SESSION() {
    this.session = null;
  }

  @Mutation
  SET_IN_LOGIN(value: boolean) {
    this.inLogin = value;
  }

  @Mutation
  SET_ENROLLMENT(value: { universityId: string; enrollment: Enrollment }) {
    this.sessions[value.universityId].enrollments.push(value.enrollment);
  }

  @Mutation
  CHANGE_USER_DATA(value: { firstName: string; lastName: string; email: string }) {
    /* eslint-disable @typescript-eslint/camelcase */
    if (this.session) {
      this.session.user.given_name = `${value.firstName} ${value.lastName}`;
      this.session.user.email = value.email;
    }
  }
}
