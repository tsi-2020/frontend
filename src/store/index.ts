import { University } from "@/models/university";
import Vue from "vue";
import Vuex from "vuex";
import VuexPersist from "vuex-persist";
import UserModule from "./modules/user";

Vue.use(Vuex);

type State = {
  sidebarVisible: boolean;
  university: null | University;
  userModule?: UserModule;
};

const persist = new VuexPersist({
  storage: window.localStorage,
  reducer: (state: State) => ({
    userModule: {
      session: state.userModule?.session,
      sessions: state.userModule?.sessions
    }
  })
});

export default new Vuex.Store<State>({
  state: {
    sidebarVisible: false,
    university: null
  },
  mutations: {
    SET_UNIVERSITY(state, value) {
      state.university = value;
    },
    SET_SIDEBARVISIBLE(state, value) {
      state.sidebarVisible = value;
    }
  },
  actions: {
    logout: context => {
      context.commit("userModule/CLEAR_SESSION", context.state.userModule?.session?.user.Tenant);
      location.reload();
    }
  },
  modules: {
    userModule: UserModule
  },
  plugins: [persist.plugin]
});
