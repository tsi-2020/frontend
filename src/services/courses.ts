import axios from "@/plugins/axios";
import { Course } from "@/models/course";

export async function getCourses(universityId: string, subjectName: string): Promise<Course[]> {
  let params = {};
  if (subjectName) {
    params = { subjectName: subjectName };
  }
  const response = await axios.get<Course[]>(`/api/Universities/${universityId}/Subjects`, { params: params });
  return response.data;
}

export async function getCourse(courseId: string): Promise<Course> {
  const response = await axios.get<Course>(`/api/Subjects/${courseId}`);
  return response.data;
}
