import axios from "@/plugins/axios";
import { GetThreads, Thread, GetMessages, Message } from "@/models/thread";

export async function getThreads(forumId: string, filters: { page: number; limit: number }): Promise<GetThreads> {
  const response = await axios.get<GetThreads>(`/api/Threads/${forumId}`, {
    params: { Page: filters.page, Limit: filters.limit }
  });
  return response.data;
}

export async function createThread(forumId: string, title: string): Promise<Thread> {
  const response = await axios.post<Thread>(`/api/Forums/${forumId}/Threads`, { title });
  return response.data;
}

export async function deleteThread(threadId: string): Promise<Thread> {
  const response = await axios.delete<Thread>(`/api/Threads/${threadId}`);
  return response.data;
}

export async function createMessage(threadId: string, text: string): Promise<Message> {
  const response = await axios.post<Message>(`/api/Message/${threadId}`, { text });
  return response.data;
}

export async function getMessages(threadId: string, filters: { page: number; limit: number }): Promise<GetMessages> {
  const response = await axios.get<GetMessages>(`/api/Threads/${threadId}/messages`, {
    params: { Page: filters.page, Limit: filters.limit }
  });
  return response.data;
}
