import axios from "@/plugins/axios";
import { University } from "@/models/university";

export async function getUniversityByPath(path: string): Promise<University> {
  const response = await axios.get<University[]>("/api/Universities", { params: { path } });
  return response.data[0];
}

export async function getUniversities(): Promise<University[]> {
  const response = await axios.get<University[]>("/api/Universities");
  return response.data;
}
