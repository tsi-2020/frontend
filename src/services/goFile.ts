import axios from "@/plugins/axios";

async function getServer(): Promise<string> {
  const response = await axios.get<{ status: string; data: { server: string } }>("https://apiv2.gofile.io/getServer");
  return response.data.data.server;
}

type FileReponse = {
  data: {
    code: string;
    file: { name: string };
  };
};

export function activeGofile(code: string): Promise<unknown> {
  return axios.get(`https://apiv2.gofile.io/getUpload?c=${code}`);
}

export async function uploadFile(formData: FormData): Promise<string> {
  const server = await getServer();
  const response = await axios.post<FileReponse>(`https://${server}.gofile.io/uploadFile`, formData, {
    headers: {
      "Content-Type": "multipart/form-data"
    }
  });
  activeGofile(response.data.data.code).catch();
  return `https://${server}.gofile.io/download/${response.data.data.code}/${response.data.data.file.name}`;
}
