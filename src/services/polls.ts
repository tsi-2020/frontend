import axios from "@/plugins/axios";
import { NewPollType, Poll, ReplyPoll, PollAnswer } from "@/models/poll";

export async function createPoll(universityId: string, subjectId: string, poll: NewPollType): Promise<void> {
  await axios.post(`/api/Universities/${universityId}/Subjects/${subjectId}/Polls`, poll);
}

export async function publishPoll(universityId: string, subjectId: string, pollId: string): Promise<void> {
  await axios.post(`/api/Universities/${universityId}/Subjects/${subjectId}/Polls/${pollId}`);
}

export async function getPolls(subjectId: string): Promise<Poll[]> {
  const response = await axios.get<Poll[]>(`/api/Subjects/${subjectId}/Polls`);
  return response.data;
}

export async function getPoll(universityId: string, pollId: string): Promise<NewPollType> {
  const response = await axios.get<NewPollType>(`/api/University/${universityId}/PublishedPolls/${pollId}`);
  return response.data;
}

export async function replyCoursePoll(subjectId: string, pollId: string, reply: ReplyPoll): Promise<void> {
  await axios.post(`/api/Subjects/${subjectId}/Polls/${pollId}/Reply`, reply);
}

export async function replyUniversityPoll(universityId: string, pollId: string, reply: ReplyPoll): Promise<void> {
  await axios.post(`/api/Universities/${universityId}/Polls/${pollId}/Reply`, reply);
}

export async function getPublishedPolls(subjectId: string, userid: string): Promise<Poll[]> {
  const response = await axios.get<Poll[]>(`/api/Subjects/${subjectId}/PublishedPolls`, { params: { userId: userid } });
  return response.data;
}

export async function getPollAnwers(
  pollId: string,
  filters: { page: number; limit: number }
): Promise<{ count: number; readPollAnswerDtos: PollAnswer[] }> {
  const response = await axios.get<{ count: number; readPollAnswerDtos: PollAnswer[] }>(
    `/api/Polls/${pollId}/Answers`,
    { params: { page: filters.page, limit: filters.limit } }
  );
  return response.data;
}

export async function getUniversityPolls(universityId: string, userId: string): Promise<Poll[]> {
  const response = await axios.get<Poll[]>(`/api/University/${universityId}/PublishedPolls`, {
    params: { userId: userId }
  });
  return response.data;
}
