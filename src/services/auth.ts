import axios from "@/plugins/axios";
import store from "@/store";
import jwtDecode from "jwt-decode";

export async function authenticate(
  user: {
    ci: number;
    password: string;
  },
  universityId: string
): Promise<{ accessToken: string; refreshToken: string }> {
  const response = await axios.post(`/api/Auth/${universityId}/login`, user);
  return response.data;
}

export async function getNewToken(): Promise<{ accessToken: string; refreshToken: string }> {
  if (store.state.userModule?.session?.tokens.refreshToken) {
    const response = await axios.post<{ accessToken: string; refreshToken: string }>("/api/Auth/refreshToken", {
      refreshToken: store.state.userModule.session.tokens.refreshToken
    });
    const data = { tokens: response.data, user: jwtDecode(response.data.accessToken) };
    store.commit("userModule/SET_SESSION", data);
    return response.data;
  } else {
    throw new Error("Missing credentials");
  }
}
