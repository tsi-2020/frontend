import axios from "@/plugins/axios";
import { StudentsEnrollments, ProfessorsEnrollments } from "@/models/enrollment";

export async function getEnrolledProfessors(subjectId: string): Promise<ProfessorsEnrollments[]> {
  const response = await axios.get<{ professors: ProfessorsEnrollments[] }>(`/api/Subjects/${subjectId}/Enrollments`);
  return response.data.professors;
}

export async function getEnrolledStudents(subjectId: string): Promise<StudentsEnrollments[]> {
  const response = await axios.get<{ students: StudentsEnrollments[] }>(`/api/Subjects/${subjectId}/Enrollments`);
  return response.data.students;
}

export async function removeProfessor(subjectId: string, enrollmentId: string): Promise<void> {
  await axios.delete(`/api/Subjects/${subjectId}/Enrollments/${enrollmentId}`);
}
