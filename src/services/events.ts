import axios from "@/plugins/axios";
import { CreateEvent, Event } from "@/models/event";

export async function getEvents(courseId: string, startDate: Date, endDate: Date): Promise<Event[]> {
  const response = await axios.get<Event[]>(`/api/Subjects/${courseId}/CalendarEvents`, {
    params: { StartDate: startDate, EndDate: endDate }
  });
  return response.data;
}

export async function createEvent(courseId: string, event: CreateEvent): Promise<Event> {
  const response = await axios.post<Event>(`/api/Subjects/${courseId}/CalendarEvents`, event);
  return response.data;
}
