import axios from "@/plugins/axios";
import { User } from "@/models/user";
import { Enrollment } from "@/models/enrollment";

export async function getUsers(fullName: string): Promise<User[]> {
  const response = await axios.get<{ count: number; userList: User[] }>("/api/Users/", {
    params: { FullNameFilter: fullName }
  });
  return response.data.userList;
}

export async function validateToken(token: string): Promise<User> {
  const response = await axios.get<User>(`/api/Users/ValidateToken/${token}`);
  return response.data;
}

export async function resetPassword(token: string, password: string): Promise<User> {
  const response = await axios.post<User>(`/api/Users/ResetPassword/${token}`, { password });
  return response.data;
}

export async function recoverPassword(universityId: string, ci: string): Promise<void> {
  await axios.post(`/api/Auth/${universityId}/ResetPassword`, { ci });
}

export async function getEnrollments(userId: string): Promise<Enrollment[]> {
  const response = await axios.get<Enrollment[]>(`api/Users/${userId}/Enrollments`);
  return response.data;
}

export async function enroll(courseId: string): Promise<void> {
  await axios.post(`/api/Subjects/${courseId}/enroll`);
}
