import axios from "@/plugins/axios";
import { Forum } from "@/models/forum";

export async function getForum(forumId: string): Promise<Forum> {
  const response = await axios.get<Forum>(`/api/Forums/${forumId}`);
  return response.data;
}
