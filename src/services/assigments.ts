import axios from "@/plugins/axios";
import { Assigment } from "@/models/assigment";

export async function getAssigment(subjectId: string, assigmentId: string): Promise<Assigment> {
  const response = await axios.get<Assigment>(`/api/Subjects/${subjectId}/Assignments/${assigmentId}`);
  return response.data;
}

export async function uploadAssignment(
  subjectId: string,
  assigmentId: string,
  data: { fileUrl: string; fileName: string }
): Promise<Assigment> {
  const response = await axios.post<Assigment>(
    `/api/Subjects/${subjectId}/Assignments/${assigmentId}/deliveries`,
    data
  );
  return response.data;
}
