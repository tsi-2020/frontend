import axios from "@/plugins/axios";
import { Career } from "@/models/career";

export async function getCareers(universityId: string, careerName?: string): Promise<Career[]> {
  const response = await axios.get<Career[]>(`/api/Universities/${universityId}/Careers`, { params: { careerName } });
  return response.data;
}

export async function getCareer(universityId: string, careerId: string): Promise<Career> {
  const response = await axios.get<Career>(`/api/Universities/${universityId}/Careers/${careerId}`);
  return response.data;
}
