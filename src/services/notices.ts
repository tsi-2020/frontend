import axios from "@/plugins/axios";
import { Notice } from "@/models/notice";

export async function getNoticesByCourse(universityId: string, courseId: string): Promise<Notice[]> {
  const response = await axios.get<Notice[]>(`/api/Universities/${universityId}/Subjects/${courseId}/Notices`);
  return response.data;
}

export async function getNoticesOfUniversity(universityId: string): Promise<Notice[]> {
  const response = await axios.get<Notice[]>(`/api/Universities/${universityId}/Notices`);
  return response.data;
}

export async function getNotice(noticeId: string): Promise<Notice> {
  const response = await axios.get<Notice>(`/api/Notices/${noticeId}`);
  return response.data;
}
