import axios from "@/plugins/axios";
import { GetProfessor, UpdateProfessor } from "@/models/professor";

export async function getProfessor(professorId: string | undefined): Promise<GetProfessor> {
  const response = await axios.get<GetProfessor>(`/api/Users/${professorId}`);
  return response.data;
}

export async function updateProfessor(professor: UpdateProfessor, professorId: string): Promise<void> {
  return axios.put(`/api/Users/${professorId}`, professor);
}
