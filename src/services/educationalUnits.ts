import axios from "@/plugins/axios";
import { EducationalUnit } from "@/models/educationalUnit";

export async function createEducationalUnit(
  courseId: string,
  educationalUnit: EducationalUnit
): Promise<EducationalUnit> {
  const response = await axios.post<EducationalUnit>(`/api/Subjects/${courseId}/EducationalUnit`, educationalUnit);
  return response.data;
}

export async function updateEducationalUnit(
  courseId: string,
  unitId: string,
  educationalUnit: EducationalUnit
): Promise<EducationalUnit> {
  const response = await axios.put<EducationalUnit>(
    `/api/Subjects/${courseId}/EducationalUnit/${unitId}`,
    educationalUnit
  );
  return response.data;
}

export async function deleteEducationalUnit(courseId: string, unitId: string): Promise<void> {
  await axios.delete(`/api/Subjects/${courseId}/EducationalUnit/${unitId}`);
}
