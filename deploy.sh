#!/usr/bin/env bash
export IMAGE=${CONTAINER_IMAGE}:${CI_BUILD_REF}

envsubst < templates/deployK8S.yml > .generated/deployK8S.yml 

cat .generated/deployK8S.yml 

kubectl apply -f .generated/

